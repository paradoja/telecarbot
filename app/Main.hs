{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Configuration.Dotenv (defaultConfig, loadFile)
import Control.Applicative ((<|>))
import Control.Monad.Trans (liftIO)
import Data.Aeson
import Data.Proxy
import Data.Text as Text (Text, concat, pack)
import qualified Data.Text.Read as Text
import GHC.Generics
import Network.HTTP.Client (Manager, defaultManagerSettings, newManager)
import Servant.API
import Servant.Client
import qualified Telegram.Bot.API as Telegram
import Telegram.Bot.Simple
import Telegram.Bot.Simple.Debug
import Telegram.Bot.Simple.UpdateParser

-- | Bot conversation state model.
newtype BotModel = BotModel
  { httpManager :: Manager
  }

-- | Actions bot can perform.
data Action
  = -- | Perform no action.
    NoAction
  | TellBrands
  | TellModels Int
  | ErrorMsg Text
  deriving (Show)

-- | Bot application.
bot :: Manager -> BotApp BotModel Action
bot mgr =
  BotApp
    { botInitialModel = BotModel mgr,
      botAction = flip handleUpdate,
      botHandler = handleAction,
      botJobs = []
    }

data Brand = Brand {id :: Int, brand :: Text} deriving (Show, Generic)

instance FromJSON Brand

data CarModel = CarModel {id :: Int, model :: Text, brand :: Brand} deriving (Show, Generic)

instance FromJSON CarModel

type CarApi =
  "brands" :> Get '[JSON] [Brand]
    :<|> "models" :> QueryParam "brandId" Int :> Get '[JSON] [CarModel]

brands :: ClientM [Brand]
models :: Maybe Int -> ClientM [CarModel]
brands :<|> models = client api

api :: Proxy CarApi
api = Proxy

-- | How to process incoming 'Telegram.Update's
-- and turn them into 'Action's.
handleUpdate :: BotModel -> Telegram.Update -> Maybe Action
handleUpdate _ =
  parseUpdate $
    TellBrands <$ command "brands"
      <|> (processModels <$> command "models")

processModels txt = case Text.decimal txt of
  Right (i, _) -> TellModels $ fromIntegral i
  _ -> ErrorMsg "chacho, el brand id que me diste es chungo"

-- | How to handle 'Action's.
handleAction :: Action -> BotModel -> Eff Action BotModel
handleAction action model =
  case action of
    NoAction -> pure model
    TellBrands ->
      model <# do
        let mgr = httpManager model
        brands' <- liftIO $ runQuery mgr brands
        let brands = case brands' of
              Right b -> b
              Left _ -> []
        let text = Text.concat $ fmap (\(Brand id name) -> name <> " has id " <> (pack . show $ id) <> "\n") $ brands
        replyText $ "brands: \n" <> text
        pure NoAction
    TellModels id ->
      model <# do
        let mgr = httpManager model
        models' <- liftIO $ runQuery mgr $ models (Just id)
        let ms = case models' of
              Right b -> b
              Left _ -> []
        let text = Text.concat $ (\(CarModel _ m _) -> m <> "\n") <$> ms
        replyText $ "modelitos:\n" <> text
        pure NoAction
    ErrorMsg e ->
      model <# do
        replyText $ "MEEEECCCCC! " <> e
        pure NoAction

runQuery mgr query =
  runClientM
    query
    ( mkClientEnv
        mgr
        (BaseUrl Http "the-vehicles-api.herokuapp.com" 80 "")
    )

-- | Run bot using 'Telegram.Token' from @TELEGRAM_BOT_TOKEN@ environment.
main :: IO ()
main = do
  -- if doenv is present, load it, but respect current env variables
  _ <- loadFile defaultConfig

  manager' <- newManager defaultManagerSettings

  token <- getEnvToken "TELEGRAM_BOT_TOKEN"
  env <- Telegram.defaultTelegramClientEnv token
  startBot_ (bot manager') env
